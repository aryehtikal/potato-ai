import logging
import os

from slack_bolt.adapter.socket_mode import SocketModeHandler
from slack import WebClient
from slack_bolt import App

from dotenv import load_dotenv,find_dotenv
load_dotenv(find_dotenv())

SLACK_BOT_TOKEN=os.getenv('SLACK_BOT_TOKEN')
SLACK_APP_TOKEN=os.getenv('SLACK_APP_TOKEN')

# Event API & Web API
app = App(token=SLACK_BOT_TOKEN)
client = WebClient(SLACK_BOT_TOKEN)


log_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
logging.basicConfig(format=log_format, level=logging.ERROR)

logger = logging.getLogger()

# This gets activated when the bot is tagged in a channel
@app.event("app_mention")
def handle_message_events(body, logger):
    # Log message
    print(str(body["event"]["text"]).split(">")[1])

    # Create prompt for ChatGPT
    prompt = str(body["event"]["text"]).split(">")[1]

    # Let the user know that we are busy with the request
    response = client.chat_postMessage(channel=body["event"]["channel"],
                                       thread_ts=body["event"]["event_ts"],
                                       text=f"Hello from your Ai-Potatoes! :robot_face: \nThanks for your request, I'm on it!")

    # ask potato AI
    try:
        from potato_ai import potato_ai
        logger.info("got objective: %s", prompt)
        potato_ai_response = potato_ai(prompt)
    except Exception as e:
        logger.exception("call to langchain failed")
        potato_ai_response = f"failed to get answer: {e}"

    # Reply to thread
    response = client.chat_postMessage(channel=body["event"]["channel"],
                                       thread_ts=body["event"]["event_ts"],
                                       text=f"Here you go: \n{potato_ai_response}")


if __name__ == "__main__":
    SocketModeHandler(app, SLACK_APP_TOKEN).start()
